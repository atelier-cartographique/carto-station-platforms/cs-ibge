import { makeRecord } from "sdi/source";
import { fromRecord } from "sdi/locale";

export function attribution() {
  return `Bruxelles Environnement \nLeefmilieu Brussel`;
}

export function credits() {
  return fromRecord(
    makeRecord(
      "Fond de plan: Brussels UrbIS ®© - CIRB - CIBG",
      "Achtergrond: Brussels UrbIS ®© - CIRB - CIBG"
    )
  );
}
